package testes;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import agenda.pessoa.Pessoa;


public class TestAgenda {
	
	private Pessoa umaPessoa;
	
        @Before
        public void setUp() throws Exception {
        umaPessoa = new Pessoa();
        }

	@Test
	public void testSetGetNome(){
		umaPessoa.setNome("Dylan");
		assertEquals("Dylan", umaPessoa.getNome());
	}
	@Test
	public void testSetGetTelefone(){
		umaPessoa.setTelefone("555-555");
		assertEquals("555-555", umaPessoa.getTelefone());
	}
	@Test
	public void testSetGetCPF(){
		umaPessoa.setCPF("1234-1234");
		assertEquals("1234-1234", umaPessoa.getCPF());
	}
	@Test
	public void testSetGetRG(){
		umaPessoa.setRG("1234");
		assertEquals("1234", umaPessoa.getRG());
	}
	@Test
	public void testSetGetEmail(){
		umaPessoa.setEmail("emailteste")
		assertEquals("emailteste", umaPessoa.getEmail());
	}
	@Test
	public void testSetGetEndereco(){
		umaPessoa.setEndereco("Gama");
		assertEquals("Gama", umaPessoa.getEndereco());
	}
	public void testSetGetSexo(){
		umaPessoa.setSexo("M");
		assertEquals("M", umaPessoa.getSexo());
	}
	public void testSetGetIdade(){
		umaPessoa.setIdade("Quinze");
		assertEquals("Quinze", umaPessoa.getIdade());
	}
	public void testSetGetHangout(){
		umaPessoa.setHangout("HangoutTeste");
		assertEquals("HangoutTeste", umaPessoa.getHangout());
	}
}
